/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatvalidator;

import com.hcdc.coedp.analysis.RecordFolderAnalyser;
import java.io.File;
import org.apache.log4j.Logger;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class FormatValidatorMain {

    /**
     * @param args the command line arguments
     */
    // TODO code application logic here
    public static void main(String[] args)
            /*    */ throws Exception /*    */ {
        /* 38 */ File dir = new File("C:\\Users\\user\\Desktop\\ERM API's\\New folder");
        /*    */
 /* 40 */ String unziplocation = "C:\\Users\\admin\\.org\\appdata";
        /*    */
 /* 42 */ String schemalocation = "C:\\Users\\admin\\.org\\schema";
        /*    */
 /* 44 */ String jhoveConfig = "C:\\Users\\admin\\.org\\conf\\jhove.conf";
        /*    */
 /* 46 */ String pidxsdPath = "C:\\Users\\admin\\.org\\conf\\eGOV-PID.xsd";
 
        /* 47 */ Logger logger = Logger.getLogger(FormatValidatorMain.class);
        
        /* 48 */ RecordFolderAnalyser analyser = new RecordFolderAnalyser(jhoveConfig, pidxsdPath, schemalocation, unziplocation, logger);
        
        /* 49 */ String recordFolderReport = analyser.analyse(dir);
        
        /* 50 */ logger.info(recordFolderReport);
        
        /*    */    }
}
