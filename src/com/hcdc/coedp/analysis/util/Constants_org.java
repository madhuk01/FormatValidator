/*    */ package com.hcdc.coedp.analysis.util;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class Constants_org
/*    */ {
/*    */   public static final String REPORT_FILE_NAME = "analysis_result.json";
/*    */   public static final String TRANSFERRED_DIR_NAME = "Transferred";
/*    */   public static final String ANALYSED_DIR_NAME = "Analysed";
/* 30 */   public static HashMap<String, String> REFER_LIB_BY_FORMAT_MAP = new HashMap() { } ;
/*    */ }