/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hcdc.coedp.analysis.util;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import javax.swing.KeyStroke;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class Constants {
    public static final String APPLICATION_NAME = "File Validate Tool";
    public static final String CARD_DASHBOARD = "startCard";
    public final static String MENU_ITEM_OTPUTPATH = "Output Path";
    public final static String MENU_ITEM_BASIC_CONFIGURATION = "Basic Configuration";
    public final static String MENU_BASIC_CONFIG = "Basic Configuration";
    public final static String MENU_ITEM_SHOW_DASHBOARD = "FVT Home";
    public final static String MENU_ITEM_HELP = "Help";
    public final static String MENU_ITEM_EXIT = "Exit";
    public static final KeyStroke KEY_SHOW_DASHBOARD = KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK);//CTRL+H;
    public static final KeyStroke KEY_HELP = KeyStroke.getKeyStroke("F1");//F1
     //EXIT
    public static final KeyStroke KEY_EXIT = KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK);//ALT+F4
    
    /**
     * values for Format Extension
     */
    public static final String FORMAT_PDF_EXTENSION = ".pdf";
    public static final String FORMAT_DOC_EXTENSION = ".doc";
    public static final String FORMAT_DOCX_EXTENSION = ".docx";
    public static final String FORMAT_PPT_EXTENSION = ".ppt";
    public static final String FORMAT_PPTX_EXTENSION = ".pptx";
    public static final String FORMAT_XLS_EXTENSION = ".xls";
    public static final String FORMAT_XLSX_EXTENSION = ".xlsx";
    public static final String FORMAT_ODT_EXTENSION = ".odt";
    public static final String FORMAT_ODS_EXTENSION = ".ods";
    public static final String FORMAT_ODP_EXTENSION = ".odp";
    public static final String FORMAT_ODF_EXTENSION = ".odf";
    public static final String FORMAT_RTF_EXTENSION = ".rtf";
    public static final String FORMAT_TXT_EXTENSION = ".txt";
    public static final String FORMAT_XML_EXTENSION = ".xml";
    public static final String FORMAT_TIFF_EXTENSION = ".tif";
    public static final String FORMAT_JPG_EXTENSION = ".jpg";
    public static final String FORMAT_CSV_EXTENSION = ".csv";
    public static final String FORMAT_ZIP_EXTENSION = ".zip";
    public static final String FORMAT_EML_EXTENSION = ".eml";
    public static final String FORMAT_MSG_EXTENSION = ".msg";
    /**
     * Format MIME Type
     */
    public static final String FORMAT_PDF_MIMETYPE = "application/pdf";
    public static final String FORMAT_DOC_MIMETYPE = "application/msword";
    public static final String FORMAT_DOCX_MIMETYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String FORMAT_XLS_MIMETYPE = "application/vnd.ms-excel";
    public static final String FORMAT_XLSX_MIMETYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String FORMAT_PPT_MIMETYPE = "application/vnd.ms-powerpoint";
    public static final String FORMAT_PPTX_MIMETYPE = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    public static final String FORMAT_ODT_MIMETYPE = "application/vnd.oasis.opendocument.text";
    public static final String FORMAT_ODS_MIMETYPE = "application/vnd.oasis.opendocument.spreadsheet";
    public static final String FORMAT_ODP_MIMETYPE = "application/vnd.oasis.opendocument.presentation";
    public static final String FORMAT_ODF_MIMETYPE = "application/vnd.oasis.opendocument.formula";
    public static final String FORMAT_RTF_MIMETYPE = "application/rtf";
    public static final String FORMAT_TXT_MIMETYPE = "text/plain";
    public static final String FORMAT_XML_MIMETYPE = "text/xml";
    public static final String FORMAT_TIFF_MIMETYPE = "image/tiff";
    public static final String FORMAT_JPG_MIMETYPE = "image/jpeg";
    public static final String FORMAT_CSV_MIMETYPE = "text/csv";
    public static final String FORMAT_ZIP_MIMETYPE = "application/zip";
    public static final String FORMAT_EML_MIMETYPE = "message/rfc822";//standard mime type ...but eml can have multiple mime types 
    public static final String FORMAT_MSG_MIMETYPE = "application/vnd.ms-outlook";
    /**
     * values for Format
     */
    public static final String FORMAT_PDF = "PDF";
    public static final String FORMAT_DOC = "DOC";
    public static final String FORMAT_XML = "XML";
    public static final String FORMAT_TIFF = "TIFF";
    public static final String FORMAT_JPEG = "JPEG";
    public static final String FORMAT_PDFA = "PDFA";
    public static final String FORMAT_TXT = "TXT";
    public static final String FORMAT_DOCX = "DOCX";
    public static final String FORMAT_PPTX = "PPTX";
    public static final String FORMAT_PPT = "PPT";
    public static final String FORMAT_XLSX = "XLSX";
    public static final String FORMAT_XLS = "XLS";
    public static final String FORMAT_RTF = "RTF";
    public static final String FORMAT_ODP = "ODP";
    public static final String FORMAT_ODF = "ODF";
    public static final String FORMAT_ODS = "ODS";
    public static final String FORMAT_ODT = "ODT";
    public static final String FORMAT_CSV = "CSV";
    public static final String FORMAT_ZIP = "ZIP";
    public static final String FORMAT_OPC = "OPC";
    public static final String FORMAT_JPG = "JPG";
    public static final String FORMAT_TIF = "TIF";
    public static final String FORMAT_MSG = "MSG"; //email format
    public static final String FORMAT_EML = "EML";//email format
    /**
     * PDF Profiles Name
     */
    public static final String PDF_VERSION = "PDF 1.0 - 1.6";
    public static final String PDFA_1b = "PDF/A 1b";
    public static final String PDFA_1a = "PDF/A 1a";
    public static final String LINEARIZED_PDF = "Linearized PDF";
    public static final String TAGGED_PDF = "Tagged PDF";
    public static final String REPORT_FILE_NAME = "analysis_result.json";
    public static HashMap<String, String> REFER_LIB_BY_FORMAT_MAP = new HashMap() { } ;
}
