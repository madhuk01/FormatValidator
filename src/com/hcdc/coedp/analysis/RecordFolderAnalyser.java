package com.hcdc.coedp.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hcdc.coedp.format.validator.FormatValidator;
import com.hcdc.coedp.format.validator.generic.IValidator;
import com.hcdc.coedp.safe.commons.domain.AnalysisReport;
import com.hcdc.coedp.safe.commons.domain.FileValidation;
import com.hcdc.coedp.safe.sip.validation.SipValidation;
import edu.harvard.hul.ois.jhove.RepInfo;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.xml.sax.SAXException;

public class RecordFolderAnalyser {

    private String jhoveConfig;
    /*  65 */    private SipValidation sipValidation = new SipValidation();
    /*  66 */    private String basePath = null;
    /*  67 */    private String relativePath = null;
    /*  68 */    private FileValidation validation = null;
    /*  69 */    private RepInfo info = null;
    private Logger logger;
    private Gson gson;
    private String source;
    private String PIDXSDPath;
    private String schemaLocation;
    private String unzipLocation;

    public RecordFolderAnalyser(String jhoveConfig, String PIDXSDPath, String schemaLocation, String unzipLocation, Logger logger) {
        /*  87 */ this.jhoveConfig = jhoveConfig;
        /*  88 */ this.PIDXSDPath = PIDXSDPath;
        /*  89 */ this.gson = new GsonBuilder().setPrettyPrinting().create();
        /*  90 */ this.schemaLocation = schemaLocation;
        /*  91 */ this.unzipLocation = unzipLocation;
        /*  92 */ this.logger = logger;
    }

    private void setBasePath(String basePath) {
        /* 101 */ this.basePath = basePath;
    }

    public String analyse(File recordFolder)
            throws IOException {
        
        /* 115 */ String jSONReport = null;

        /* 117 */ if (recordFolder != null) {
            /* 119 */ if (recordFolder.exists()) {
                    System.out.println("Inside analyser if");
                /* 120 */ setBasePath(recordFolder.getAbsolutePath());
                /* 122 */ AnalysisReport report = new AnalysisReport();
                /* 124 */ report.setLastModified(recordFolder.lastModified());
                /* 126 */ report.setProcessStartTS(Calendar.getInstance().getTimeInMillis());
                /* 131 */ int contentCount = 0;
                /* 132 */ boolean isContentEmpty = (contentCount = recordFolder.list().length) == 0;
                /* 133 */ if (isContentEmpty) {
                    /* 135 */ report.setDirEmpty(true);
                } else {
                    
                    System.out.println("Inside analyser else");
                    
                    /* 138 */ File[] contentFiles = recordFolder.listFiles();
                    /* 140 */ report.setTotalItems(contentCount);
                    /* 142 */ List fileValidations = new ArrayList();
                    /* 143 */ List tempFileValidation = null;
                    /* 144 */ for (File contentFile : contentFiles) {
                        /* 145 */ if (contentFile.isDirectory()) {
                            /* 146 */ File[] insideFiles = contentFile.listFiles();
                            /* 147 */ for (File insideFile : insideFiles) {
                                /* 148 */ tempFileValidation = validate(insideFile, this.schemaLocation, this.unzipLocation);
                                /* 149 */ fileValidations.addAll(tempFileValidation);
                            }
                        } else {
                            /* 152 */ tempFileValidation = validate(contentFile, this.schemaLocation, this.unzipLocation);
                            /* 153 */ fileValidations.addAll(tempFileValidation);
                        }
                        //  // this.validation.setNote(tempFileValidation);
                        for (int i = 0; i < tempFileValidation.size(); i++) {
                            System.out.println(tempFileValidation.get(i));
                        }
                    }
                    /* 160 */ report.setFileValidations(fileValidations);
                }
                /* 162 */ report.setProcessEndTS(Calendar.getInstance().getTimeInMillis());
                try {
                    /* 164 */ jSONReport = this.gson.toJson(report);
                    // this.validation.setNote("Json report:");
                    // // this.validation.setNote(report);
                } catch (Exception exception) {
                    /* 166 */ this.logger.error("Exception in RecordFolderAnalyser: while converting as json:: ", exception);
                }
            }
        }
        /* 170 */ return jSONReport;
    }

    private List<FileValidation> validate(File inputFile, String schemaLocation, String unzipLocation)
            throws IOException {
        /* 189 */ List listValidation = new ArrayList();
        /* 190 */ if (inputFile != null) {
            /* 191 */ if (inputFile.isDirectory()) {
                /* 196 */ File[] files = inputFile.listFiles();
                /* 197 */ List tempListValidation = null;
                /* 198 */ for (File anotherInputFile : files) {
                    /* 199 */ tempListValidation = validate(anotherInputFile, schemaLocation, unzipLocation);
                    /* 200 */ listValidation.addAll(tempListValidation);
                }
                /* 202 */            } else if (inputFile.isFile()) {
                /* 203 */ this.validation = new FileValidation();
                /* 205 */ this.validation.setFileName(inputFile.getName());
                /* 207 */ this.source = inputFile.getAbsolutePath();
                /* 208 */ this.relativePath = this.source.replace(this.basePath, "");
                /* 209 */ if (this.relativePath.equals(File.separator + inputFile.getName())) {
                    /* 210 */ this.relativePath = ".";
                } else {
                    /* 212 */ this.relativePath = this.relativePath.substring(this.relativePath.indexOf(File.separator), this.relativePath.length());
                    /* 213 */ this.relativePath = this.relativePath.substring(this.relativePath.indexOf(File.separator), this.relativePath.lastIndexOf(File.separator) + 1);
                    /* 214 */ this.relativePath = this.relativePath.replace("\\", "/");
                }
                /* 216 */ this.validation.setRelativePath(this.relativePath);

                /* 227 */ int dotPos = inputFile.toString().lastIndexOf(".");
                /* 228 */ String extension = inputFile.toString().substring(dotPos + 1);
                /* 230 */      //   if (Constants.REFER_LIB_BY_FORMAT_MAP.containsKey(extension)) {

                /* 231 */        // String key = (String)Constants.REFER_LIB_BY_FORMAT_MAP.get(extension);
                String key = extension;
                String[] exts = new String[]{"jpg", "jpeg", "tiff", "pdf", "xml", "ascii", "sql","tiff","xml"};
                String[] exts1 = new String[]{"xlsx", "pptx", "docx"};
                if (Arrays.asList(exts).contains(key)) {
                    key = "jhove";
                } else if (Arrays.asList(exts1).contains(key)) {
                    key = "ooxml";
                }
                Tika tika;
                String mimetype;
                /* 232 */ switch (key) {
                    case "jhove":
                        if (extension.equalsIgnoreCase(".sql")) {
                            extension = "ASCII";
                        }
                        /* 234 */ this.info = this.sipValidation.valid(inputFile, extension + "-hul", this.jhoveConfig);
                        /* 238 */ if (inputFile.getName().endsWith("_pid.xml")) {
                            /* 239 */ if (validateXMLSchema(this.PIDXSDPath, inputFile, this.logger)) {
                                /* 240 */ this.validation.setValid("Valid");
                                /* 241 */                 // this.validation.setNote("Schema: Valid against EGov-PID Schema");
                            } else {
                                /* 243 */ this.validation.setValid("Invalid");
                                /* 244 */                 // this.validation.setNote("Schema: Invalid against EGov-PID Schema");
                            }
                            /* 246 */                        } else if (inputFile.getName().endsWith(".xml")) {
                            /* 247 */ if (validateXMLWithoutSchema(inputFile, this.logger)) {
                                /* 248 */ this.validation.setValid("Valid");
                                /* 249 */                 // this.validation.setNote("Wellformed: Xml Structure v   alid");
                            } else {
                                /* 251 */ this.validation.setValid("Invalid");
                                /* 252 */                 // this.validation.setNote("Wellformed: Xml Structure invalid");
                            }
                        } else {
                            /* 255 */ this.validation.setValid(this.info.getValid() == 0 ? "Invalid" : this.info.getValid() == 1 ? "Valid" : "Unpredictable");
                            /* 256 */ if ((inputFile.getName().endsWith(".txt")) || (inputFile.getName().endsWith(".eml")) || (inputFile.getName().endsWith(".rtf")) || (inputFile.getName().endsWith(".msg"))) {
                            } /* 257 */ // this.validation.setNote("Identification: Found as text file");
                            /* 258 */ else if (inputFile.getName().endsWith(".pdf")) {
                            } /* 259 */ // this.validation.setNote("checked Header signature: %PDF-m.n <br/> Dictionary: %%EOF <br/> well formed: Version information, Dates, File specifications, annotations and XMP data");
                            /* 260 */ else if ((inputFile.getName().endsWith(".tiff")) || (inputFile.getName().endsWith(".tif"))) {
                            } /* 261 */ // this.validation.setNote("checked Header signature: 0x4D4D002A(big-endian) or 0x49492A00 (if little-endian) <br/>tag: ImageLength(257), ImageWidth (256), Photometric Interpretation (262),DateTime(306) tag  ");
                            /* 262 */ else if ((inputFile.getName().endsWith(".jpeg")) || (inputFile.getName().endsWith(".jpg"))) {
                            } /* 263 */ // this.validation.setNote("checked Header signature: 0xFF, 0xD8, 0xFF <br/> formatted segments: markers 0xC0, 0xFE are recognized.  <br/>");
                            else {
                                /* 265 */                 // this.validation.setNote("");
                            }
                        }
                        /* 268 */ this.validation.setWellFormed(this.info.getWellFormed() + "");
                        /* 269 */ this.validation.setFormat(extension.toUpperCase());
                        /* 270 */ this.validation.setMimeType(this.info.getMimeType());
                        /* 271 */ this.validation.setProfiles(this.info.getProfile());
                        /* 272 */ break;
                    case "ods":
                    case "odp":
                    case "odf":
                    case "odt":
                        /* 274 */ IValidator ivalidator = FormatValidator.getODFValidator(schemaLocation, unzipLocation);
                        /* 275 */ if (ivalidator.validate(inputFile)) /* 276 */ {
                            this.validation.setValid("Valid");
                        } else {
                            /* 278 */ this.validation.setValid("Invalid");
                        }
                        /* 280 */             // this.validation.setNote("ODF Version: " + ODFValidator.getOdfVersion() + "<br/>Schema: checked against odf schema " + ODFValidator.getOdfVersion());
/* 281 */ this.validation.setWellFormed("1");

                        /* 283 */ tika = new Tika();
                        /* 284 */ mimetype = tika.detect(inputFile);
                        /* 285 */ this.validation.setMimeType(mimetype);
                        /* 286 */ this.validation.setFormat(extension.toUpperCase());
                        /* 287 */ break;
                    case "ooxml":
                        /* 289 */ if (FormatValidator.getOOXMLValidator().validate(inputFile)) {
                            /* 290 */ this.validation.setValid("Valid");
                        } /* 292 */ else if (inputFile.getName().endsWith(".docx")) /* 293 */ {
                            this.validation.setValid("Valid");
                        } else {
                            /* 295 */ this.validation.setValid("Invalid");
                        }

                        /* 298 */             // this.validation.setNote("Schema: OOXML schema validated");
/* 299 */ this.validation.setWellFormed("1");
                        /* 300 */ tika = new Tika();
                        /* 301 */ mimetype = tika.detect(inputFile);
                        /* 302 */ this.validation.setMimeType(mimetype);
                        /* 303 */ this.validation.setFormat(extension.toUpperCase());
                        /* 304 */ break;
                    case "custom":
                        /* 306 */ this.validation.setValid("Valid");
                        /* 307 */             // this.validation.setNote("Checked Mime type");
/* 308 */ this.validation.setWellFormed("1");
                        /* 309 */ tika = new Tika();
                        /* 310 */ mimetype = tika.detect(inputFile);
                        /* 311 */ this.validation.setMimeType(mimetype);
                        /* 312 */ this.validation.setFormat(extension.toUpperCase());
                }

                //   }
                /* 318 */ this.validation.setCreated(fileCreation(inputFile));
                /* 319 */ Date lastModifiedDate = new Date(inputFile.lastModified());
                /* 320 */ this.validation.setLastModified(lastModifiedDate);

                /* 322 */ this.validation.setSize(inputFile.length());

                /* 324 */ listValidation.add(this.validation);
            }
        }
        /* 327 */ return listValidation;
    }

    private Date fileCreation(File inputFile)
            throws IOException {
        /* 339 */ Date creationDate = null;
        /* 340 */ FileSystem fs = FileSystems.getDefault();
        /* 341 */ Path item = fs.getPath(inputFile.getAbsolutePath(), new String[0]);
        /* 342 */ BasicFileAttributes attrs = Files.readAttributes(item, BasicFileAttributes.class, new LinkOption[0]);
        /* 343 */ creationDate = new Date(attrs.creationTime().toMillis());
        /* 344 */ return creationDate;
    }

    public static boolean validateXMLSchema(String xsdPath, File xmlFile, Logger logger) {
        try {
            /* 358 */ SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            /* 359 */ Schema schema = factory.newSchema(new File(xsdPath));
            /* 360 */ Validator validator = schema.newValidator();
            /* 361 */ validator.validate(new StreamSource(xmlFile));
        } catch (IOException | SAXException e) {
            /* 363 */ logger.error("Exception in RecordFolderAnalyser.validateXMLWithoutSchema(): ", e);
            /* 364 */ return false;
        }
        /* 366 */ return true;
    }

    public static boolean validateXMLWithoutSchema(File xmlFile, Logger logger) {
        try {
            /* 379 */ DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            /* 380 */ factory.setValidating(false);
            /* 381 */ factory.setNamespaceAware(true);
            /* 382 */ DocumentBuilder builder = factory.newDocumentBuilder();
            /* 383 */ builder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            /* 385 */ logger.error("Exception in RecordFolderAnalyser.validateXMLWithoutSchema(): ", e);
            /* 386 */ return false;
        }
        /* 388 */ return true;
    }
}

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.analysis.RecordFolderAnalyser
 * JD-Core Version:    0.6.2
 */
