package com.hcdc.coedp.format.validator.generic;

import java.io.File;

public abstract interface IValidator
{
  public abstract boolean validate(File paramFile);
}

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.generic.IValidator
 * JD-Core Version:    0.6.2
 */