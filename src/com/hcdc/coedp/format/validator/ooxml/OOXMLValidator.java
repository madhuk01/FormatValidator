/*    */ package com.hcdc.coedp.format.validator.ooxml;
/*    */ 
/*    */ import com.hcdc.coedp.format.validator.generic.IValidator;
/*    */ import java.io.File;
/*    */ import java.io.FileInputStream;
/*    */ import java.io.IOException;
/*    */ import java.util.UUID;
/*    */ import org.probatron.officeotron.OOXMLValidationSession;
/*    */ import org.probatron.officeotron.ReportFactory;
/*    */ import org.probatron.officeotron.ValidationReport;
/*    */ import org.probatron.officeotron.sessionstorage.Store;
/*    */ 
/*    */ public class OOXMLValidator
/*    */   implements IValidator
/*    */ {
/*    */   public boolean validate(File sourceFile)
/*    */   {
/* 33 */     OOXMLValidationSession ovs = null;
/* 34 */     UUID uuid = null;
/* 35 */     Store.init(System.getProperty("java.io.tmpdir"), false);
/*    */     try {
/* 37 */       uuid = Store.putZippedResource(new FileInputStream(sourceFile), sourceFile.getPath());
/*    */     } catch (IOException ex) {
/* 39 */       ex.printStackTrace();
/*    */     }
    /* 41 */ ovs = new OOXMLValidationSession(uuid, new ReportFactory()
/*    */     {
/*    */       public ValidationReport create()
/*    */       {
/* 45 */       // return new OOXMLValidator.HelperValidationReport(OOXMLValidator.this, null);
                return new OOXMLValidator.HelperValidationReport();
/*    */       }
/*    */     });
/* 48 */     
                if (ovs != null) {
/* 49 */       ovs.validate();
/* 50 */       if (ovs.getErrCount() == 1) {
/* 51 */         return true;
/*    */       }
/* 53 */       return false;
/*    */     }
/* 56 */     return false;
/*    */   }
/*    */ 
/*    */   private class HelperValidationReport implements ValidationReport
/*    */   {
/*    */     
            private HelperValidationReport()
/*    */     {
/*    */    
             }
/*    */ 
/*    */    @Override
     public void addComment(String s)
/*    */     {
/*    */     }
/*    */ 
/*    */     public void addComment(String klass, String s)
/*    */     {
/*    */     }
/*    */ 
/*    */     public void decIndent()
/*    */     {
/*    */     }
/*    */ 
/*    */     public void endReport() {
/*    */     }
/*    */ 
/*    */     public int getErrCount() {
/* 81 */       return 0;
/*    */     }
/*    */ 
/*    */     public void incErrs()
/*    */     {
/*    */     }
/*    */ 
/*    */     public void incIndent()
/*    */     {
/*    */     }
/*    */ 
/*    */     public void streamOut()
/*    */       throws IOException
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.ooxml.OOXMLValidator
 * JD-Core Version:    0.6.2
 */