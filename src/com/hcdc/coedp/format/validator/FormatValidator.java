/*    */ package com.hcdc.coedp.format.validator;
/*    */ 
/*    */ import com.hcdc.coedp.format.validator.generic.IValidator;
/*    */ import com.hcdc.coedp.format.validator.odf.ODFValidator;
/*    */ import com.hcdc.coedp.format.validator.ooxml.OOXMLValidator;
/*    */ 
/*    */ public class FormatValidator
/*    */ {
/*    */   private static IValidator odfValidator;
/*    */   private static IValidator ooXMLValidator;
/*    */ 
/*    */   public static IValidator getODFValidator(String schemaLocation, String unzipLocation)
/*    */   {
/* 36 */     if (odfValidator == null) {
/* 37 */       odfValidator = new ODFValidator(schemaLocation, unzipLocation);
/*    */     }
/* 39 */     return odfValidator;
/*    */   }
/*    */ 
/*    */   public static IValidator getOOXMLValidator()
/*    */   {
/* 48 */     if (ooXMLValidator == null) {
/* 49 */       ooXMLValidator = new OOXMLValidator();
/*    */     }
/* 51 */     return ooXMLValidator;
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.FormatValidator
 * JD-Core Version:    0.6.2
 */