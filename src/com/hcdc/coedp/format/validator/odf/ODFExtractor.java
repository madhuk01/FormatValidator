/*    */ package com.hcdc.coedp.format.validator.odf;
/*    */ 
/*    */ import java.io.File;
/*    */ import net.lingala.zip4j.core.ZipFile;
/*    */ import net.lingala.zip4j.exception.ZipException;
/*    */ 
/*    */ public class ODFExtractor
/*    */   implements IExtractor
/*    */ {
/*    */   public File unzipFile(String sourceFile, String unzipLocation)
/*    */     throws ZipException
/*    */   {
/* 35 */     String destination = unzipLocation + sourceFile.substring(sourceFile.lastIndexOf(File.separator) + 1, sourceFile.lastIndexOf("."));
/*    */     try {
/* 37 */       ZipFile zipFile = new ZipFile(sourceFile);
/* 38 */       zipFile.extractAll(destination);
/* 39 */       return new File(destination);
/*    */     }
/*    */     catch (ZipException e) {
/* 42 */       throw e;
/*    */     }
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.ODFExtractor
 * JD-Core Version:    0.6.2
 */