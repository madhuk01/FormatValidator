package com.hcdc.coedp.format.validator.odf;

import java.io.File;

public abstract interface IVersionChecker
{
  public abstract String checkVersion(File paramFile);
}

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.IVersionChecker
 * JD-Core Version:    0.6.2
 */