/*    */ package com.hcdc.coedp.format.validator.odf.util;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.FileFilter;
/*    */ 
/*    */ public class ContentXmlFileFilter
/*    */   implements FileFilter
/*    */ {
/*    */   public boolean accept(File contextXml)
/*    */   {
/* 24 */     if ("content.xml".equalsIgnoreCase(contextXml.getName())) {
/* 25 */       return true;
/*    */     }
/* 27 */     return false;
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.util.ContentXmlFileFilter
 * JD-Core Version:    0.6.2
 */