/*    */ package com.hcdc.coedp.format.validator.odf.util;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.FileFilter;
/*    */ 
/*    */ public class ODFSchemaFileFilter
/*    */   implements FileFilter
/*    */ {
/*    */   private String version;
/*    */   private ODFSchemaType type;
/*    */ 
/*    */   public String getVersion()
/*    */   {
/* 31 */     return this.version;
/*    */   }
/*    */ 
/*    */   public void setVersion(String version)
/*    */   {
/* 40 */     this.version = version;
/*    */   }
/*    */ 
/*    */   public ODFSchemaType getType()
/*    */   {
/* 49 */     return this.type;
/*    */   }
/*    */ 
/*    */   public void setType(ODFSchemaType type)
/*    */   {
/* 58 */     this.type = type;
/*    */   }
/*    */ 
/*    */   public ODFSchemaFileFilter(String version, ODFSchemaType type)
/*    */   {
/* 68 */     this.version = version;
/* 69 */     this.type = type;
/*    */   }
/*    */ 
/*    */   public boolean accept(File file)
/*    */   {
/* 80 */     if (file.getName().contains(this.version + "-" + this.type.getType())) {
/* 81 */       return true;
/*    */     }
/* 83 */     return false;
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.util.ODFSchemaFileFilter
 * JD-Core Version:    0.6.2
 */