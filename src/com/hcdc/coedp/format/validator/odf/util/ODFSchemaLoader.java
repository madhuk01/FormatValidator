/*    */ package com.hcdc.coedp.format.validator.odf.util;
/*    */ 
/*    */ import java.io.File;
/*    */ 
/*    */ public class ODFSchemaLoader
/*    */ {
/*    */   public static File loadManifestSchema(String version, String schemaLocation)
/*    */   {
/* 22 */     File[] files = load(ODFSchemaType.MANIFEST, version, schemaLocation);
/* 23 */     if ((files != null) && (files.length > 0)) {
/* 24 */       return files[0];
/*    */     }
/* 26 */     return null;
/*    */   }
/*    */ 
/*    */   public static File loadODFSchema(String version, String schemaLocation)
/*    */   {
/* 37 */     File[] files = load(ODFSchemaType.ODF, version, schemaLocation);
/* 38 */     if ((files != null) && (files.length > 0)) {
/* 39 */       return files[0];
/*    */     }
/* 41 */     return null;
/*    */   }
/*    */ 
/*    */   private static File[] load(ODFSchemaType schematype, String version, String schemaLocation)
/*    */   {
/* 54 */     return new File(schemaLocation).listFiles(new ODFSchemaFileFilter(version, schematype));
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.util.ODFSchemaLoader
 * JD-Core Version:    0.6.2
 */