/*    */ package com.hcdc.coedp.format.validator.odf.util;
/*    */ 
/*    */ public enum ODFSchemaType
/*    */ {
/* 12 */   MANIFEST("os-manifest-schema"), 
/* 13 */   ODF("os-schema");
/*    */ 
/*    */   private String type;
/*    */ 
/* 17 */   private ODFSchemaType(String type)
            { 
                this.type = type; 
            }
/*    */ 
/*    */   public String getType()
/*    */   {
/* 21 */     return this.type;
/*    */   }
/*    */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.util.ODFSchemaType
 * JD-Core Version:    0.6.2
 */