/*     */ package com.hcdc.coedp.format.validator.odf;

/*     */
 /*     */ import com.hcdc.coedp.format.validator.generic.IValidator;
/*     */ import com.hcdc.coedp.format.validator.odf.util.ContentXmlFileFilter;
/*     */ import com.hcdc.coedp.format.validator.odf.util.MetaXmlFileFilter;
/*     */ import com.hcdc.coedp.format.validator.odf.util.ODFSchemaLoader;
/*     */ import com.hcdc.coedp.format.validator.odf.util.ODFSchemaType;
/*     */ import com.thaiopensource.util.PropertyMapBuilder;
/*     */ import com.thaiopensource.validate.ValidateProperty;
/*     */ import com.thaiopensource.validate.ValidationDriver;
/*     */ import com.thaiopensource.xml.sax.ErrorHandlerImpl;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.nio.file.Files;
/*     */ import net.lingala.zip4j.exception.ZipException;
import org.apache.log4j.Logger;
/*     */ import org.xml.sax.ErrorHandler;
/*     */ import org.xml.sax.InputSource;
/*     */ import org.xml.sax.SAXException;

/*     */
 /*     */ public class ODFValidator
        /*     */ implements IValidator /*     */ {

    /*     */ private static String odfVersion;
    /*     */    private final ODFVersionChecker versionChecker;
    /*     */    private final ODFExtractor extractor;
    /*     */    private final String schemaLocation;
    /*     */    private final String unzipLocation;
    private Logger logger;

    /*     */
 /*     */ public ODFValidator(String schemaLocation, String unzipLocation) /*     */ {
        /*  50 */ this.schemaLocation = schemaLocation;
        /*  51 */ this.unzipLocation = unzipLocation;
        /*  52 */ this.versionChecker = new ODFVersionChecker();
        /*  53 */ this.extractor = new ODFExtractor();
        /*     */    }

    /*     */
 /*     */
    @Override
    public boolean validate(File sourceFile) /*     */ {
        /*  65 */ boolean isValid = false;
        /*  66 */ File extractedDirectory = null;
        /*     */ try {
            /*  68 */ extractedDirectory = this.extractor.unzipFile(sourceFile.getAbsolutePath(), this.unzipLocation);

            File[] files = extractedDirectory.listFiles(new ContentXmlFileFilter());
            /*  71 */ File contentXmlFile = null;
            /*  72 */ File metaXmlFile = null;
            /*  73 */ if (files.length > 0) {
                /*  74 */ contentXmlFile = files[0];
                /*     */            }
            /*  76 */ File mainfestXMLFile = new File(extractedDirectory, "META-INF" + File.separator + "manifest.xml");
            /*     */
 /*  78 */ odfVersion = this.versionChecker.checkVersion(contentXmlFile);
            /*  79 */ if (odfVersion == null) {
                /*  80 */ files = extractedDirectory.listFiles(new MetaXmlFileFilter());
                /*  81 */ if (files.length > 0) {
                    /*  82 */ metaXmlFile = files[0];
                    /*     */                }
                /*  84 */ odfVersion = this.versionChecker.checkVersionFromMeta(metaXmlFile);
                /*     */            }
            /*  86 */ if (odfVersion == null) {
                /*  87 */ odfVersion = "1.2";
                /*     */            }
            /*  91 */ ODFSchemaType type = ODFSchemaType.MANIFEST;
            //  logger.info("::Ordinal value::"+type.ordinal());
            
            System.out.println(odfVersion+" "+ODFSchemaType.MANIFEST+" "+this.schemaLocation);
            
            File mainFestSchemaFile = getODFSchemaFile(odfVersion, ODFSchemaType.MANIFEST, this.schemaLocation);
            
            System.out.println("-- manifest file name--"+mainFestSchemaFile);
            
            /*  92 */ File odfSchemaFile = getODFSchemaFile(odfVersion, ODFSchemaType.ODF, this.schemaLocation);
            /*  94 */ ErrorHandler seh = new ErrorHandlerImpl();
            /*  95 */ PropertyMapBuilder builder = new PropertyMapBuilder();
            /*  96 */ builder.put(ValidateProperty.ERROR_HANDLER, seh);
            /*  97 */ ValidationDriver validator = new ValidationDriver(builder.toPropertyMap());
            /*     */
 /*  99 */ InputSource contentXmlSource = ValidationDriver.fileInputSource(contentXmlFile);
            /* 100 */ InputSource mainfestXMLSource = ValidationDriver.fileInputSource(mainfestXMLFile);
            /* 101 */ InputSource manifestSchemaSource = ValidationDriver.fileInputSource(mainFestSchemaFile);
            /* 102 */ InputSource odfSchemaSource = ValidationDriver.fileInputSource(odfSchemaFile);
            /* 103 */ validator.loadSchema(manifestSchemaSource);
            /* 104 */ isValid = validator.validate(mainfestXMLSource);
            /*     */
 /* 106 */ if (isValid) {
                /* 107 */ validator.loadSchema(odfSchemaSource);
                /* 108 */ for (File file : extractedDirectory.listFiles()) {
                    /* 109 */ String mimeType = Files.probeContentType(file.toPath());
                    /* 110 */ if ("application/xml".equalsIgnoreCase(mimeType)) /* 111 */ {
                        isValid = validator.validate(contentXmlSource);
                    }
                    /*     */                }
                /*     */            }
            /*     */        } /*     */ catch (IOException | SAXException | ZipException ex) {
            /* 116 */ ex.printStackTrace();
            /*     */        } finally {
            /* 118 */ if ((extractedDirectory != null) && (extractedDirectory.exists())) {
                /* 119 */        // delete(extractedDirectory);
/*     */            }
            /*     */        }
        /* 122 */ return isValid;
        /*     */    }
///*     */ 1.$SwitchMap$com$hcdc$coedp$format$validator$odf$util$ODFSchemaType[type.ordinal()]
/*     */ private File getODFSchemaFile(String odfVersion, ODFSchemaType type, String schemaLocation) /*     */ {
        /* 136 */   //  switch (1.$SwitchMap$com$hcdc$coedp$format$validator$odf$util$ODFSchemaType[type.ordinal()]) {
        
            System.out.println(" Ordinal type: "+type.ordinal());
        
        switch (type.ordinal()) {
            /*     */ case 0:
                /* 138 */ return ODFSchemaLoader.loadManifestSchema(odfVersion, schemaLocation);
            /*     */ case 1:
                /* 140 */ return ODFSchemaLoader.loadODFSchema(odfVersion, schemaLocation);
            /*     */        }
        /* 142 */ return null;

        /*     */    }

    /*     */
 /*     */ public static void delete(File file) /*     */ {
        if (file.isDirectory()) /*     */ {
            /* 154 */ if (file.list().length == 0) {
                /* 155 */ file.delete();
                /*     */            } /*     */ else {
                /* 158 */ String[] files = file.list();
                /* 159 */ for (String temp : files) /*     */ {
                    /* 161 */ File fileDelete = new File(file, temp);
                    /*     */
 /* 163 */ delete(fileDelete);
                    /*     */                }
                /*     */
 /* 166 */ if (file.list().length == 0) {
                    /* 167 */ file.delete();
                    /*     */                }
                /*     */            }
            /*     */        } /*     */ else /*     */ {
            /* 173 */ file.delete();
            /*     */        }
        /*     */    }

    /*     */
 /*     */ public static String getOdfVersion() /*     */ {
        /* 184 */ return odfVersion;
        /*     */    }
    /*     */ }

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.ODFValidator
 * JD-Core Version:    0.6.2
 */
