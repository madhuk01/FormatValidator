/*    */ package com.hcdc.coedp.format.validator.odf;
/*    */ 
/*    */ import java.io.File;
/*    */ import javax.xml.parsers.DocumentBuilder;
/*    */ import javax.xml.parsers.DocumentBuilderFactory;
/*    */ import javax.xml.xpath.XPath;
/*    */ import javax.xml.xpath.XPathConstants;
/*    */ import javax.xml.xpath.XPathExpression;
/*    */ import javax.xml.xpath.XPathFactory;
/*    */ import org.w3c.dom.Document;
/*    */ import org.w3c.dom.Node;
/*    */ 
/*    */ public class ODFVersionChecker
/*    */   implements IVersionChecker
/*    */ {
/*    */   public String checkVersion(File contentFile)
/*    */   {
/* 29 */     DocumentBuilder documentBuilder = null;
/* 30 */     DocumentBuilderFactory factory = null;
/* 31 */     String version = null;
/*    */  
            try {
/* 33 */       factory = DocumentBuilderFactory.newInstance();
/* 34 */       factory.setNamespaceAware(true);
/* 35 */       documentBuilder = factory.newDocumentBuilder();
/* 36 */       Document doc = documentBuilder.parse(contentFile);

              /*  
/* 37        XPath xPath = XPathFactory.newInstance().newXPath();
/* 39        String expression = "//*[local-name()='document-content']";
/* 40        Node node = (Node)xPath.compile(expression).evaluate(doc, XPathConstants.NODE); */
              
              // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();
              
              XPathExpression expr =
                xpath.compile("//*[local-name()='document-content']");
                Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);
/* 41 */        version = node.getAttributes().getNamedItem("office:version").getNodeValue();
                System.out.println("--Version found--"+version);
/*    */     } catch (Exception ex) {
/* 43 */       ex.printStackTrace();
/*    */     }
/* 45 */     return version;
/*    */   }
/*    */ 
/*    */   public String checkVersionFromMeta(File metaFile)
/*    */   {
/* 55 */     DocumentBuilder documentBuilder = null;
/* 56 */     DocumentBuilderFactory factory = null;
/* 57 */     String version = null;
/*    */     try {
/* 59 */       factory = DocumentBuilderFactory.newInstance();
/* 60 */       factory.setNamespaceAware(true);
/* 61 */       documentBuilder = factory.newDocumentBuilder();
/* 62 */       Document doc = documentBuilder.parse(metaFile);
/* 63 */       XPath xPath = XPathFactory.newInstance().newXPath();
/*    */ 
/* 65 */       String expression = "//*[local-name()='document-meta']";
/* 66 */       Node node = (Node)xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
/* 67 */       version = node.getAttributes().getNamedItem("office:version").getNodeValue();
/*    */     } catch (Exception ex) {
/* 69 */       ex.printStackTrace();
/*    */     }
/* 71 */     return version;
/*    */   }
/*    */ }