package com.hcdc.coedp.format.validator.odf;

import java.io.File;
import net.lingala.zip4j.exception.ZipException;

public abstract interface IExtractor
{
  public abstract File unzipFile(String paramString1, String paramString2)
    throws ZipException;
}

/* Location:           D:\Main\Projects\org-preserve\lib\ERM_FORMAT_VALIDATOR\FormatValidator.jar
 * Qualified Name:     com.hcdc.coedp.format.validator.odf.IExtractor
 * JD-Core Version:    0.6.2
 */